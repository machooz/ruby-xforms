describe "iText" do

  let(:xform1_path) { File.expand_path '../data/xform_itext.xml', __FILE__ }
  let(:form) { XForms::Form.parse_file xform1_path }
  before(:each) { I18n.locale = :es }

  it "gets label from itext" do
    form.controls[0].label.should == 'Texto'
  end

  it "gets label from itext for item" do
    form.controls[1].items.first.label.should == 'Texto'
  end

  it "gets label from empty itext" do
    form.controls[2].label.should == ''
  end

end