require 'time'

module XForms

  class ::Object
    def dyn_value; self; end
  end

  class XPathRef
    attr_accessor :form
    attr_accessor :path

    def initialize(form, path)
      @form = form
      @path = path
    end

    def dyn_value
      form.model_instance.xpath(path, form.namespaces, self)
    end

    def itext id
      form.itext[I18n.locale.to_s][id] || ''
    end

    def now
      Time.now.utc.iso8601
    end
  end

  module Label
    attr_accessor :label

    def label
      @label.dyn_value
    end
  end

  class Control
    attr_accessor :form
    attr_accessor :binding
    attr_accessor :readonly
    attr_accessor :constraint
    attr_accessor :constraint_message
    attr_accessor :relevant
    attr_accessor :type
    include Label

    def initialize(form)
      @form = form
      @readonly = false
    end

    def value=(value)
      value = Integer(value).to_s if type == 'int'
      form.model_instance.xpath(binding).first.children = value
    end

    def value
      data = form.model_instance.xpath(binding).first.inner_text
      return data unless type

      case type
      when 'int' then Integer(data) rescue 0
      else data
      end
    end

    def readonly
      @readonly.dyn_value
    end

    def valid?
      return true unless constraint
      node = form.model_instance.xpath(binding).first
      node.xpath(constraint) == true
    end

    def relevant?
      return true unless @relevant
      @relevant.dyn_value
    end
  end

  class Input < Control
  end

  class Select1 < Control
    attr_accessor :items

    def initialize(form)
      super
      @items = []
    end
  end

  class Item
    attr_accessor :form
    attr_accessor :value
    include Label

    def initialize(form, label = nil, value = nil)
      @form = form
      @label = label
      @value = value
    end
  end
end