module XForms
  class Form
    attr_accessor :model_instance
    attr_accessor :itext
    attr_accessor :controls
    attr_accessor :namespaces
    attr_accessor :calculates

    def initialize
      @controls = []
      @namespaces = {}
      @calculates = []
    end

    def self.parse_file(path)
      File.open(path, 'r') do |f|
        parse f
      end
    end

    def self.parse(input)
      FormParser.new(input).parse
    end

    def recalculate
      calculates.each do |calc|
        value = calc[:value].dyn_value
        model_instance.xpath(calc[:binding]).first.children = value.to_s
      end
    end
  end
end
