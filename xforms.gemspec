# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "xforms/version"

Gem::Specification.new do |s|
  s.name        = "xforms"
  s.version     = XForms::VERSION
  s.authors     = ["Juan Wajnerman", "Ary Borenszweig"]
  s.email       = ["jwajnerman@manas.com.ar", "aborenszweig@manas.com.ar"]
  s.homepage    = "https://bitbucket.org/instedd/ruby-xforms"
  s.summary     = %q{XForms 1.1 implementation}
  s.description = %q{This gem can be used to parse and fill XForms 1.1}

  s.rubyforge_project = "xforms"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec"
  s.add_runtime_dependency "nokogiri"
  s.add_runtime_dependency "i18n"
end
